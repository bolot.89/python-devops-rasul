from PIL import Image
import os, argparse


def resize_image(file_name, file_size, resizefile):
    try:
        img = Image.open(os.getcwd() + "/" + file_name)
        img.thumbnail(file_size, Image.ANTIALIAS)
        img.save("{}_{}".format(file_size, resizefile))
        img.close()
    except IOError:
        print("cannot create resized image for {}".format(img))    


arg_parse = argparse.ArgumentParser()
arg_parse.add_argument("--filename", action="store", type=str, help="image to resize")
arg_parse.add_argument("--width", action="store", type=int, help="width of resized image")
arg_parse.add_argument("--height", action="store", type=int, help="height of resized image")
arg_parse.add_argument("--resizefile", action="store", type=str, help="new name of resized image")
args = arg_parse.parse_args()

if args.filename:
    if not os.path.isfile(args.filename):
        exit('File {} not found'.format(args.filename))
    file_name = args.filename    
else:
    exit("You entered wrong arguments")

if args.height < 0 or args.width < 0:
    exit("Height or width can't be less than zero")

resize_image(args.filename, (args.height, args.width), args.resizefile)    