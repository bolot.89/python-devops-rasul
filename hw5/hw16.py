from geopy.geocoders import Nominatim
from hw17 import *
import json, ast, argparse

def print_geo_data(file_name):
    try:
        geolocator = Nominatim(user_agent="test");
        geo_data = retrieve_coordinates(file_name);
        location = geolocator.reverse((geo_data['Latitude'], geo_data['Longitude']))
        print('Input data: {}, {}'.format(str(location.latitude), str(location.longitude)))
        print('Location: {}'.format(location.address))
        print('Google Maps URL: https://www.google.com/maps/search/?api=1&query={},{}'.format(str(location.latitude), str(location.longitude)))
    except ValueError:
        print("Error getting data")

arg_parse = argparse.ArgumentParser()
arg_parse.add_argument("--filename", action="store", type=str, help="image name")

args = arg_parse.parse_args()

if args.filename:
    if not os.path.isfile(args.filename):
        exit('File {} not found'.format(args.filename))
    file_name = args.filename    
else:
    exit("You entered wrong arguments")

print_geo_data(file_name)