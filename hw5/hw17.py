from GPSPhoto import gpsphoto
import json, os

def retrieve_coordinates(jpg_file):
    try:
        image = gpsphoto.getGPSData(jpg_file)
        gps_data = {};
        for tag in image.keys():
            if tag == "Latitude":
                gps_data[tag] = str(image[tag])
            elif tag == "Longitude":
                gps_data[tag] = str(image[tag])
        
        return gps_data
    except IOError:
        print("error reading data {}".format(jpg_file))         