import socket, traceback, sys, re

host = 'localhost'
port = 51423
custom_hosts = {
    "example.com": "123.123.1.5"
}

def ip_is_valid(address):
    parts = address.split(".")
    if len(parts) != 4:
        return False
    for item in parts:
        if not 0 <= int(item) <= 255:
            return False
    return True

def is_valid_hostname(hostname):
    if hostname[-1] == ".":
        hostname = hostname[:-1]
    if len(hostname) > 253:
        return False

    labels = hostname.split(".")

    if re.match(r"[0-9]+$", labels[-1]):
        return False

    allowed = re.compile(r"(?!-)[a-z0-9-]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(label) for label in labels)    


def make_command(data):
    if "ADD" in data:
        (_, host_ip_pair) = data.split(" ")
        (host, ip_address) = host_ip_pair.split(":")
        if ip_is_valid(ip_address) and is_valid_hostname(host):
            custom_hosts[host] = ip_address
            return ("Add", True, host, ip_address)
        else:
            return ("Add", False, None, None)
    else:
        if is_valid_hostname(data):
            if custom_hosts.get(data) is not None:
                return ("Found", True, data, custom_hosts.get(data))
            else:   
                return ("Found", False, data, None)    
        else:     
            return ("Found", False, data, None)


try: 
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind((host, port))
except(socket.error, (value,message)):
    if s:
        s.close()
    print("Could not open socket:{}".format(message))
    sys.exit(1)

while 1:
    try:
        message, address = s.recvfrom(5000)
        message = message.decode('utf-8').replace("\n", "")
       
        (command, response, host, ip_address) = make_command(message);

        if command == "Add" and response == True:
            response_mg = "Host {} and IP {} successfuly added to DNS\n".format(host, ip_address)
            response_mg = response_mg.encode("UTF-8")
            s.sendto(response_mg, address)
        elif command == "Add" and response == False:    
            response_mg = "Couldn't add address\n"
            response_mg = response_mg.encode("UTF-8")
            s.sendto(response_mg, address)
        elif command == "Found" and response == True:
            response_mg = "Hostname {} hash IP {}\n".format(host, ip_address)
            response_mg = response_mg.encode("UTF-8")
            s.sendto(response_mg, address)
        elif command == "Found" and response == False:
            response_mg = "Hostname {} not found\n".format(host)
            response_mg = response_mg.encode("UTF-8")
            s.sendto(response_mg, address)    

    except (KeyboardInterrupt, SystemExit):
        raise
    except:
        traceback.print_exc()