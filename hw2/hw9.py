#problem 6
print((sum([x for x in range(1, 101)]) ** 2) - sum([x ** 2 for x in range(1, 101)]))

#problem 9
print(max([a * b * c for (a, b, c) in [(a,b,c) for a in range(1,500) for b in range(a,500) for c in range(b,500) if a**2 + b**2 == c**2] if a + b + c == 1000]));

#problem 48
print(int(str(sum([a ** a for a in range(1, 1000)]))[-10:]))
