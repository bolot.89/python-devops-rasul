def recursive_operation(num):
    if num % 2 == 0:
        print(num / 2)
    else:
        print(num * 3 + 1);    

while 1:
    try:
        data = input("Enter data: ");
        if data == "cancel":
            print("Bye! ");
            break;
        recursive_operation(int(data));
    except(ValueError):        
        print("Can't convert to number");
       
    