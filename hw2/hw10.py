num = int(input("Enter num: "));

count = 0
while num >= 1:
    if num == 1:
        break;

    if num % 2 != 0:
        num = num * 3 + 1;
        count = count + 1;
    elif num % 2 == 0:
        num = num / 2;
        count = count + 1;

print(count);        