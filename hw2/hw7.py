
passwd_file = open("/home/bolot/Devops/python/hw2/passwd", "r+");
group_file = open("/home/bolot/Devops/python/hw2/group", "r+");

count_word = {};
group_data = {};

for line in group_file.readlines():
    data_array = line.rstrip().split(":");
    if data_array[-1] != '':
       group_data[data_array[0]] = data_array[-1]

passwd_lst = [];

for line in passwd_file.readlines():
    data = line.rstrip().split(":");
    passwd_lst.append((data[0], data[2]));
    if data[-1] in count_word:
        count_word[data[-1]] += 1;
    else:
        count_word[data[-1]] = 1; 

uid_lst = [];        
output_hash = {};

for key in group_data:
    for data in group_data[key].rstrip().split(","):
        buff_lst = [];
        for (user, uid) in passwd_lst:
            if data == user:
                if key in output_hash:
                    output_hash[key].append(uid);
                else:
                    buff_lst.append(uid);
                    output_hash[key] = buff_lst;

for key in output_hash:
    output_hash[key] = ",".join(output_hash[key])

output_file = open("/home/bolot/Devops/python/hw2/output.txt", "w");

for key in output_hash:
    output_file.write(key + ":" + output_hash[key] + "\n");

output_file.write("\n");

for key, value in count_word.items():
    output_file.write(key + " - " + str(value) + "\n");
  

passwd_file.close();
group_file.close();    
output_file.close();