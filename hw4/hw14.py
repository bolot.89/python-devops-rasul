import pickle

class Employee:
  def __init__(self,name,phone,salary=10000):
    self.name = name
    self.phone = phone
    self.salary = salary
  def print_salary_info(self):
    print("Employee {} gets {} Rubles".format(self.name,self.salary))
  def add_salary(self,delta=5000):
    self.salary = self.salary+delta
  def add(self,other_empl):
    new_name = self.name + "&" + other_empl.name
    new_phone = str(round( (int(self.phone) + int(other_empl.phone))/2 ))
    new_salary = self.salary + other_empl.salary
    return Employee(new_name,new_phone,new_salary)
  __add__=add


class Mary(Employee):
  def __init__(self,salary=10000):
    self.salary = salary
  def print_salary_info(self):
    print("Mary earns {} Dollars".format(self.salary))    


mary = Mary(2000);

out_file = open("/home/bolot/Devops/python/mary_class",'wb');
pickle.dump(mary, out_file);
out_file.close();

in_file = open("/home/bolot/Devops/python/mary_class", "rb");
new_mary_class = pickle.load(in_file);
in_file.close();


new_mary_class.print_salary_info();

'''

плюс модуля pickle в том сериализовать/десериализовать различные структуры данных
в поток байтов. В свою очередь, данные могут быть сохранены на жестком диске (к примеру БД) 
или пересылаться через протокол TCP. Это предоставляет дополнительные возможности для программиста
Минус данной технологии состоит в том, что она привязна к питоновским структурам данных. Например, кортеж
Соответсвенно, она не совместима с другими языками программирования. То есть, при попытки десериализации 
другими языками программирования, данные могут просто потеряться

'''