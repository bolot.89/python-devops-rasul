
class Router:
    def __init__(self):
        self.ips = [];
        self.routes = [];
        self.index_ip = None;

    def add_ip(self, ip_address, mask, interface):
        if self.ip_is_valid(ip_address) and self.ip_is_valid(mask):
            self.ips.append((ip_address, mask, interface))

    def remove_ip(self, ip_address):
        for index, (ip, _, _) in enumerate(self.ips):
            if ip == ip_address:
                self.index_ip = index   

        if self.index_ip is not None:
            print("Element removed {}".format(self.ips.pop(self.index_ip)));
        else:
            print("Element not found")    
    
    def show_ip_list(self):
        for (ip, mask, interface) in self.ips:
            print("Ip address: {} Mask: {} Interface: {}".format(ip, mask, interface))

    def add_route(self, ip_address, mask, interface):
        if self.ip_is_valid(ip_address) and self.ip_is_valid(mask):
            self.routes.append((ip_address, mask, interface))

    def remove_route(self, ip_address):
        for index, (ip, _, _) in enumerate(self.routes):
            if ip == ip_address:
                self.index_ip = index   

        if self.index_ip is not None:
            print("Element removed {}".format(self.routes.pop(self.index_ip)));
        else:
            print("Element not found")    
    
    def show_route_list(self):
        for (ip, mask, interface) in self.routes:
            print("Ip address: {} Mask: {} Interface: {}".format(ip, mask, interface))


    def ip_is_valid(self, address):
        parts = address.split(".")
        if len(parts) != 4:
            return False
        for item in parts:
            if not 0 <= int(item) <= 255:
                return False
        return True   



router = Router()
router.add_ip('192.168.133.21', '255.255.0.0', 'wlan6')
router.add_ip('192.168.133.55', '255.255.0.0', 'wlan6')
router.add_ip('192.168.133.33', '255.255.0.0', 'wlan6')
router.show_ip_list()        