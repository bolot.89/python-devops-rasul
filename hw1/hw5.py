numbers = input( "Enter sequence: " )

num_array = [ int(x) for x in numbers.split(" ") if int(x) > 0 ];

no_duplicate_array = list(set(num_array));

buffer_array = range(1,len(no_duplicate_array));

print(min(set(buffer_array)-set(no_duplicate_array)));