text = input( "Enter text: " );

word_count = {};
text_list = [];
for word in text.split(" "):
    text_list.append(word.lower());

text_freq = [text_list.count(p) for p in text_list];

frequency_dict = dict(zip(text_list, text_freq));

max_num = max(list(frequency_dict.values()));
for key, value in frequency_dict.items():
    if value == max_num:
        print("{} - {}".format(value, key))

