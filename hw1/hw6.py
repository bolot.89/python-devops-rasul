res = 0
for i in range(1, 1000000):
    ten = str(i)
    two = str(bin(i)[2:])

    if "".join(reversed(ten)) == ten and "".join(reversed(two)) == two:
        res += int(ten)

print("Sum: {}".format(res))

