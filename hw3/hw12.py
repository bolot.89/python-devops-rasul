def calculate_fibonacci(n):
    fibs = [0, 1]
    for i in range(2, n+1):
        fibs.append(fibs[-1] + fibs[-2])

    fibs.pop(0)    
    return " ".join(str(x) for x in fibs)  

num = input("Enter num: ")
print(calculate_fibonacci(int(num)))