data_type = input("Enter type for conversion (either °C or °F): ")

if data_type == "F":
    temp = float(input("Enter temperature : "));
    temp = (temp - 32) * 5.0 / 9.0;
    data_type = "°C"
elif data_type == "C":
    temp = float(input("Enter temperature : "));
    temp = 9.0/5.0 * temp + 32;
    data_type = "°F";

print("Converted temperature is {} {}".format(temp, data_type))