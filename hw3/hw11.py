def letters_range(start, end, step=1):
    alphabet = "abcdefghijklmnopqrstuvwxyz";
    alphabet_list = list(alphabet);
    start_elem = alphabet_list.index(start);
    end_elem = alphabet_list.index(end);
    print(alphabet_list[start_elem:end_elem:step]);


letters_range('b', 'w', 2);
letters_range('a', 'g');
letters_range('g', 'p');
letters_range('p', 'g', -2);
letters_range('a', 'a');